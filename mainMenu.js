let basics = require('./basic_calc')

const readline = require("readline")
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

function menu(){
    console.log(`\n\nMain Menu`)
    console.log(`1. Addition`)
    console.log(`2. Subtraction`)
    console.log(`3. Multiplication`)
    console.log(`4. Division`)
    console.log(`5. Square Root`)
    console.log(`6. Squared Square`)
    console.log(`7. Cube Volume`)
    console.log(`8. Tube Volume`)
    console.log(`0. EXIT`)

    rl.question("Please enter which operations you would like to do? ", (answer)=>{
        switch(answer){
            case "1":
                rl.question("Enter First number : ", (answer)=>{
                    rl.question("Enter second number : ", (answer1)=>{  
                        answer = parseInt(answer)
                        answer1 = parseInt(answer1)
                        basics.addition(answer, answer1)
                        rl.close()
                    })
                    })
                break;
            case "2":
                console.log('\n\SUBTRACTION (-)')
                rl.question("Enter First number : ", (answer)=>{
                    rl.question("Enter second number : ", (answer1)=>{  
                        answer = parseInt(answer)
                        answer1 = parseInt(answer1)
                        basics.subtraction(answer, answer1)
                        rl.close()
                    })
                    })
                break;
            case "3":
                console.log('\n\nMULTIPLICATION (*)')
                rl.question("Enter First number : ", (answer)=>{
                    rl.question("Enter second number : ", (answer1)=>{  
                        answer = parseInt(answer)
                        answer1 = parseInt(answer1)
                        basics.multiplication(answer, answer1)
                        rl.close()
                    })
                    })
                break;
            
            case "4":
                console.log('\n\nDIVISION (/)')
                rl.question("Enter First number : ", (answer)=>{
                    rl.question("Enter second number : ", (answer1)=>{  
                        answer = parseInt(answer)
                        answer1 = parseInt(answer1)
                        basics.division(answer, answer1)
                        rl.close()
                    })
                    })
                break;
            
            case "5":
                console.log('\n\nSQUARE ROOT (^)')
                rl.question("Enter number : ", (answer)=>{
                        answer = parseInt(answer)
                        basics.squareRoot(answer)
                        rl.close()
                    })
                break;

            case "6":
                console.log('\n\nSQUARED SQUARE')
                rl.question("Enter side (in cm) : ", (answer)=>{
                        answer = parseInt(answer)
                        basics.squaredSquare(answer)
                        rl.close()
                    })
                break;
            case "7":
                console.log('\n\nCUBE VOLUME')
                rl.question("Enter cube sides (in cm) : ", (answer)=>{
                        answer = parseInt(answer)
                        basics.cubeVolume(answer)
                        rl.close()
                    })
                break;
            case "8":
                console.log('\n\nTUBE VOLUME')
                rl.question("Enter cube HEIGHT (in cm) : ", (answer)=>{
                    rl.question("Enter cube RADIUS (in cm) : ", (answer1)=>{  
                        height = parseInt(answer)
                        radius = parseInt(answer1)
                        basics.tubeVolume(height, radius)
                        rl.close()
                    })
                    })
                break;

            default:
                break;
        }
    })
}

menu()