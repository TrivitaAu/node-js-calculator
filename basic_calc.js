module.exports.addition = (x, y) => {
    let result = x + y
    console.log(`\n\nAddition of ${x} and ${y} is = ${result}`)
}

module.exports.subtraction = (x, y) => {
    let result = x - y
    console.log(`\n\n${x} subtracted from ${y} is = ${result}`)
}

module.exports.multiplication = (x, y) => {
    let result = x * y
    console.log(`\n\nMultiplication of ${x} and ${y} is = ${result}`)
}

module.exports.division = (x, y) => {
    result = x / y
    console.log(`\n\n${x} divided by ${y} is = ${result}`)
}

module.exports.squareRoot = (x) => {
    result = Math.sqrt(x)
    console.log(`\n\nSquare root of ${x} is = ${result}`)
}

module.exports.squaredSquare = (x) => {
    result = x*x
    console.log(`\n\nSquared square with ${x} cm sides is = ${result} cm2`)
}
module.exports.cubeVolume = (x) => {
    result = x*x*x
    console.log(`\n\nCube volume with ${x} cm sides is = ${result} cm3`)
}

module.exports.tubeVolume = (height, radius) => {
    phi = parseFloat(22/7)
    result = parseFloat(phi * height * radius * radius)

    console.log(`\n\nVolume of tube with height ${height} cm and radius ${radius} cm is = ${result} cm3`)
}